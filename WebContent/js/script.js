function VisualizzaCategorie(elenco_categorie) {
	let tabella = "";

	for (let i = 0; i < elenco_categorie.length; i++) {
		tabella += creaCategoriaTab(elenco_categorie[i]);
	}

	$("#elenco-categorie").html(tabella);
}

function creaCategoriaTab(categoria) {
	let categoria_riga = '<tr identificatore ="' + categoria.id + '">';
	categoria_riga += '<td> ' + categoria.nome_categoria + '</td>';
	categoria_riga += '<td>' + categoria.codice_categoria + '</td>';
	categoria_riga += '<td>' + categoria.desc_categoria + '</td>';
	categoria_riga += '</tr>';

	return categoria_riga;
}



function VisualizzaProdotti(elenco_prodotti) {
	let tabella = "";

	for (let i = 0; i < elenco_prodotti.length; i++) {
		tabella += creaProdottoTab(elenco_prodotti[i]);
	}

	$("#elenco-prodotti").html(tabella);
}

function creaProdottoTab(prodotto) {
	let prodotto_riga = '<tr identificatore ="' + prodotto.id_prodotto + '">';
	prodotto_riga += '<td> ' + prodotto.nome_prodotto + '</td>';
	prodotto_riga += '<td>' + prodotto.codice_prodotto + '</td>';
	prodotto_riga += '</tr>';

	return prodotto_riga;
}

function PopolaTabCategorie() {
	$.ajax(
		{
			url: "http://localhost:8081/Homework9marzo/RecuperaCategorie",
			method: "GET",
			success: function(ris) {
				VisualizzaCategorie(ris);
			},
			error: function(errore) {
				console.log(errore);
			}
		}
	);
}




function PopolaTabProdotti() {
	$.ajax(
		{
			url: "http://localhost:8081/Homework9marzo/RecuperaProdotti",
			method: "POST",
			success: function(ris) {
				VisualizzaProdotti(ris);
			},
			error: function(errore) {
				console.log(errore);
			}
		}
	);
}




$(document).ready(
	function() {
		$("#cta-ElencaProdotti").click(
			function() {
				PopolaTabProdotti();
			}
		);


		$("#cta-ElencaCategorie").click(
			function() {
				PopolaTabCategorie();
			}
		);

	}
);