DROP DATABASE IF EXISTS homework_mobilificio;
CREATE DATABASE homework_mobilificio;
USE homework_mobilificio;

CREATE TABLE prodotto (
	id_prodotto INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
    nome VARCHAR (150) NOT NULL,
    codice VARCHAR (15) NOT NULL UNIQUE
    );
    

CREATE TABLE categoria (
	id_categoria INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
    nome VARCHAR (150) NOT NULL,
    descrizione VARCHAR (250),
    codice VARCHAR (15) NOT NULL UNIQUE);
    
CREATE TABLE prodotto_categoria (
	ID_prodotto INTEGER NOT NULL,
    ID_categoria INTEGER NOT NULL,
    PRIMARY KEY (ID_prodotto,ID_categoria),
    FOREIGN KEY (ID_prodotto) REFERENCES prodotto(id_prodotto),
    FOREIGN KEY (ID_categoria) REFERENCES categoria(id_categoria)
);

INSERT INTO prodotto (nome, codice) VALUES 
("Poltrona Vintage","POLT1"),
("Tavolo verde","TAV8"),
("Sedia pieghevole","SEDG1"),
("Divano rosso","DIV3"),
("Cucina della nonna", "CUC34"),
("Poggiapiedi","POGG1"),
("Quadro di Klimt","QKLI");

INSERT INTO categoria (nome, descrizione, codice) VALUES 
("Arredi da giardino", "leggeri e pieghevoli", "ARRG1"),
("Arredi da interno", "Eleganti e minimali", "ARRI1"),
("Armadi", "Capienti e miniti di specchio", "ARM1"),
("Tavoli","In pregiatissimo legno", "TAV1"),
("Sedie","Comode e pieghevoli","SED05"),
("Cucine","Cucine con elettrodomestici tecnologici","CUC12");

INSERT INTO prodotto_categoria (ID_prodotto, ID_categoria) VALUES 
(1,3),
(2,1),
(2,4),
(3,5),
(4,2),
(5,6),
(6,2),
(6,4),
(7,2);  

SELECT categoria.nome, categoria.descrizione, categoria.codice, 
prodotto.nome, prodotto.codice FROM categoria
	JOIN prodotto_categoria ON categoria.id_categoria=prodotto_categoria.ID_categoria
    JOIN prodotto ON prodotto_categoria.ID_prodotto = prodotto.id_prodotto;
    

    
