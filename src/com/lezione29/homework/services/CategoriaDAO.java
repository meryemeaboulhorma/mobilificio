package com.lezione29.homework.services;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.lezione29.homework.connections.ConnettoreDB;
import com.lezione29.homework.model.Categoria;
import com.lezione29.homework.model.Prodotto;
import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;

public class CategoriaDAO implements Dao<Categoria>{
	
	@Override
	public Categoria getById(int id) throws SQLException {
		Connection conn = (Connection) ConnettoreDB.getIstanza().getConnessione();
        
       	String query = "SELECT id_categoria, nome, descrizione, codice FROM categoria WHERE id_categoria = ?";
       	PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
       	ps.setInt(1, id);
       	
       	ResultSet risultato = ps.executeQuery();
       	risultato.next();
       	
       	Categoria categoria_temp = new Categoria ();
       	categoria_temp.setId_categoria(risultato.getInt(1));
       	categoria_temp.setNome_categoria(risultato.getString(2));
       	categoria_temp.setDesc_categoria(risultato.getString(3));
       	categoria_temp.setCodice_categoria(risultato.getString(4));
       	
       	return categoria_temp;
		
	}

	@Override
	public ArrayList<Categoria> getAll() throws SQLException {
		ArrayList<Categoria> elenco_categorie = new ArrayList<Categoria>();
	       
   		Connection conn = (Connection) ConnettoreDB.getIstanza().getConnessione();
           
       	String query = "SELECT id_categoria, nome, descrizione, codice FROM categoria";
       	PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
       	
       	ResultSet risultato = ps.executeQuery();
       	while(risultato.next()){
       		Categoria categoria_temp = new Categoria();
       		categoria_temp.setId_categoria(risultato.getInt(1));
       		categoria_temp.setNome_categoria(risultato.getString(2));
       		categoria_temp.setDesc_categoria(risultato.getString(3));
       		categoria_temp.setCodice_categoria(risultato.getString(4));
       		elenco_categorie.add(categoria_temp);
       	}
       	
       	return elenco_categorie;
	}

	@Override
	public void insert(Categoria t) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean delete(Categoria t) throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean update(Categoria t) throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}

}
