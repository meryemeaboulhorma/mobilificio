package com.lezione29.homework.services;
import java.sql.SQLException;
import java.util.ArrayList;

import com.lezione29.homework.model.Categoria;

public interface Dao<T> {
	
	T getById(int id) throws SQLException;
	ArrayList<T> getAll() throws SQLException;
	void insert (T t) throws SQLException;
	boolean delete(T t) throws SQLException;
	boolean update(T t) throws SQLException;
	
}
