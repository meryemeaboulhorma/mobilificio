package com.lezione29.homework.services;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.lezione29.homework.connections.ConnettoreDB;
import com.lezione29.homework.model.Categoria;
import com.lezione29.homework.model.Prodotto;
import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;

public class ProdottoDAO implements Dao<Prodotto>{

	@Override
	public Prodotto getById(int id_prodotto) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ArrayList<Prodotto> getAll() throws SQLException {
		ArrayList<Prodotto> elenco_prodotti = new ArrayList<Prodotto>();
	       
   		Connection conn = (Connection) ConnettoreDB.getIstanza().getConnessione();
           
       	String query = "SELECT id_prodotto, nome, codice FROM prodotto WHERE 1=1";
       	PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
       	
       	ResultSet risultato = ps.executeQuery();
       	while(risultato.next()){
       		Prodotto prodotto_temp = new Prodotto();
       		prodotto_temp.setId_prodotto(risultato.getInt(1));
       		prodotto_temp.setNome_prodotto(risultato.getString(2));
       		prodotto_temp.setCodice_prodotto(risultato.getString(3));
       		elenco_prodotti.add(prodotto_temp);
       	}
       	
       	return elenco_prodotti;
	}

	@Override
	public void insert(Prodotto t) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean delete(Prodotto t) throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean update(Prodotto t) throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}

	

	
	

}
