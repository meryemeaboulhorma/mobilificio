package com.lezione29.homework.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.lezione29.homework.model.Categoria;

import com.lezione29.homework.model.Prodotto;
import com.lezione29.homework.model.Categoria;
import com.lezione29.homework.services.CategoriaDAO;
import com.lezione29.homework.services.ProdottoDAO;


@WebServlet("/RecuperaCategorie")
public class RecuperaCategorie extends HttpServlet {       

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		
		CategoriaDAO catDAO = new CategoriaDAO();

		try {
			ArrayList<Categoria> elenco_categorie = catDAO.getAll();
			out.print(new Gson().toJson(elenco_categorie));
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
