package com.lezione29.homework.api;

import java.io.IOException;
import java.io.PrintWriter;

import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.lezione29.homework.model.Categoria;
import com.lezione29.homework.model.Prodotto;
import com.lezione29.homework.services.CategoriaDAO;
import com.lezione29.homework.services.ProdottoDAO;

@WebServlet("/RecuperaMobili")
public class RecuperaProdotti extends HttpServlet {

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		
		ProdottoDAO proDAO = new ProdottoDAO();

		try {
			ArrayList<Prodotto> elenco_prodotti = proDAO.getAll();
			out.print(new Gson().toJson(elenco_prodotti));
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
