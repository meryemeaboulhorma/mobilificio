package com.lezione29.homework.model;

public class Categoria {
	private int id_categoria;
	private String nome_categoria;
	private String desc_categoria;
	private String codice_categoria;

	public String getNome_categoria() {
		return nome_categoria;
	}

	public void setNome_categoria(String nome_categoria) {
		this.nome_categoria = nome_categoria;
	}

	public String getDesc_categoria() {
		return desc_categoria;
	}

	public void setDesc_categoria(String desc_categoria) {
		this.desc_categoria = desc_categoria;
	}
	

	public String getCodice_categoria() {
		return codice_categoria;
	}

	public void setCodice_categoria(String codi_categoria) {
		this.codice_categoria = codice_categoria;
	}
	
    
	

	public int getId_categoria() {
		return id_categoria;
	}

	public void setId_categoria(int id_categoria) {
		this.id_categoria = id_categoria;
	}

	public Categoria(String nome_categoria, String desc_categoria, String codi_categoria) {
		super();
		this.nome_categoria = nome_categoria;
		this.desc_categoria = desc_categoria;
		this.codice_categoria = codi_categoria;
	}

	public Categoria() {

	}
}
