package com.lezione29.homework.model;

import java.util.ArrayList;

public class Prodotto {
	
	private String nome_prodotto;
	private String codice_prodotto;
	private int id_prodotto;
	public ArrayList<Categoria> cat_prodotto = new ArrayList<Categoria>();
	
	public Prodotto(String nome_prodotto, String codice_prodotto, int id_prodotto, ArrayList<Categoria> cat_prodotto) {
		super();
		this.nome_prodotto = nome_prodotto;
		this.codice_prodotto = codice_prodotto;
		this.id_prodotto = id_prodotto;
		this.cat_prodotto = cat_prodotto;
	}

	public Prodotto() {
		
	}

	public String getNome_prodotto() {
		return nome_prodotto;
	}

	public void setNome_prodotto(String nome_prodotto) {
		this.nome_prodotto = nome_prodotto;
	}

	public String getCodice_prodotto() {
		return codice_prodotto;
	}

	public void setCodice_prodotto(String codice_prodotto) {
		this.codice_prodotto = codice_prodotto;
	}

	public int getId_prodotto() {
		return id_prodotto;
	}

	public void setId_prodotto(int id_prodotto) {
		this.id_prodotto = id_prodotto;
	}

	public ArrayList<Categoria> getCat_prodotto() {
		return cat_prodotto;
	}

	public void setCat_prodotto(Categoria var_categoria) {
		this.cat_prodotto.add(var_categoria);
	}
	
	
	
}